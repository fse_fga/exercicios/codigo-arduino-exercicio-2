/*
 * crc16.h
 *
 *  Created on: 18/03/2014
 *      Author: Renato Coral Sampaio
 */

#ifndef CRC16_H_
#define CRC16_H_

#include <stdint.h>

typedef union {
    short crc;
    uint8_t bytes[2];
} bytesCRC;

short CRC16(short crc, char data);
short calcula_CRC(unsigned char *commands, int size);
bool verifica_crc(unsigned char *mensagem, int size);



#endif /* CRC16_H_ */
