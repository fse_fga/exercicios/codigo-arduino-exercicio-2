#include "comunicacao.h"
#include "definicoes_gerais.h"
#include "uart.h"
#include "crc16.h"
#include "HardwareSerial.h"

extern HardwareSerial Serial1;

long le_inteiro() {
  long num_inteiro = 0;
  le_uart((char*) &num_inteiro, 4);
  return num_inteiro;
}

float le_float() {
  float num_float;
  le_uart((char*) &num_float, 4);
  return num_float;
}

int le_string(char * string_recebida) {
  int tamanho = (int) le_byte_uart();
#ifdef DEBUG  
  Serial.print("String recebida [");
  Serial.print(tamanho);
  Serial.print("]: ");
#endif  
  le_uart(string_recebida, tamanho);
  string_recebida[tamanho] = '\0';
  Serial.println(string_recebida);
  return tamanho;
}

void envia_mensagem(char * comando, unsigned char *dado, int tamanho){
    unsigned char resposta[200];
    resposta[0] = CODIGO_CLIENTE;
    resposta[1] = comando[1];
    resposta[2] = comando[2];
    memcpy(&resposta[3], dado, tamanho);
    short crc = calcula_CRC(resposta, tamanho+3);
    memcpy(&resposta[tamanho+3], &crc, 2);
    Serial1.write(resposta, tamanho+5);
}

void envia_int(char * comando, long int dado) {
    envia_mensagem(comando, (unsigned char *) &dado, 4);
}

void envia_float(char * comando, float dado) {
    envia_mensagem(comando, (unsigned char *) &dado, 4);
}

void envia_string(char * comando, char * mensagem, int tamanho) {
    unsigned char msg[100];
    msg[0] = tamanho;
    memcpy(&msg[1], mensagem, tamanho);
    msg[tamanho+1] = '\0';
    envia_mensagem(comando, msg, tamanho+2);
}