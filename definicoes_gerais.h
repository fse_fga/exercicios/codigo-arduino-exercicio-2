/*
 * definicoes_gerais.h
 *
 *  Created on: 25/02/2021
 *      Author: Renato Coral Sampaio
 */

#ifndef DEFINICOES_GERAIS_H_
#define DEFINICOES_GERAIS_H_

#include <stdint.h>

#define CODIGO_CLIENTE 0x00
#define CODIGO_SERVIDOR 0x01
#define CODIGO_CMD_SOLICITA 0x23
#define CODIGO_CMD_ESCREVE 0x16

#define CMD_SOLICITA_INT    0xA1
#define CMD_SOLICITA_FLOAT  0xA2
#define CMD_SOLICITA_STRING 0xA3

#define CMD_ENVIA_INT    0xB1
#define CMD_ENVIA_FLOAT  0xB2
#define CMD_ENVIA_STRING 0xB3

#define CMD_ERROR 0xE1
#define CMD_STRING_OK 0xC1

typedef union {
 float valor_float;
 uint8_t bytes[4];
} bytesFloat;

typedef union {
 long int valor_int;
 uint8_t bytes[4];
} bytesInt;

#endif /* DEFINICOES_GERAIS_H_ */