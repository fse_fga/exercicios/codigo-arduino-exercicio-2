#include "uart.h"

#include "HardwareSerial.h"

extern HardwareSerial Serial1;

char le_byte_uart() {
  int i = 0;
  int timeout = 0;
  while(i < 1) {
    if(Serial1.available()) {
      i++;
      return Serial1.read();
    }
    delay(1);
    timeout++;
    if(timeout > 100){
      return -1;
    }
  }
}
int le_uart(char * mensagem, int num_bytes) {
  int i = 0;
  int timeout = 0;
  while(i < num_bytes) {
    if(Serial1.available()) {
      mensagem[i] = Serial1.read();
      i++;
    }
    delay(1);
    timeout++;
    if(timeout > 100){
      return -1;
    }
  }
}