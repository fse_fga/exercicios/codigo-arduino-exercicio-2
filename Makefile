CC = arduino-cli
DEVICE = arduino:avr:micro
PROJECT = ../exercicio-2-modbus
PORT = /dev/ttyACM0
LDFLAGS =
BLDDIR = .

all: compile program

compile:
	$(CC) compile --fqbn $(DEVICE) $(PROJECT)

program:
	$(CC) upload -p $(PORT) --fqbn $(DEVICE) $(PROJECT)
